<?php

require_once("./AcceptLanguage.php");

// we don't need "fancy" HTML output with shitty CSS
header("Content-Type: text/plain");

// implement the getallheaders() function for php-fpm
if (!function_exists("getallheaders"))
{
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == "HTTP_")
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

// get all headers
$headers = getallheaders();

// sort headers for a consistent output
ksort($headers);

echo "「要求ヘッダー」" . "\n";

// pretty print all headers
foreach ($headers as $key => $val)
{
    echo "  " . $key . ": " . $val . "\n";
}

echo "\n";

// diagnostic information about the "Accept-Language" HTTP header
foreach (\Teto\HTTP\AcceptLanguage::get() as $locale)
{
    $title = "Accept-Language: " . implode('-', $locale);
    echo $title . "\n";
}

echo "\n";

?>
