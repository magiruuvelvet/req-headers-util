# Requirements

 - PHP 7 or higher
 - some random web server with PHP support

# Example Output

```plain
「要求ヘッダー」
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
  Accept-Encoding: gzip, deflate, br
  Accept-Language: ja,en-US;q=0.8,en;q=0.6
  Dnt: 1
  Host: (removed from example output)
  Upgrade-Insecure-Requests: 1
  User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Falkon/3.0.1 Chrome/69.0.3497.128 Safari/537.36

Accept-Language: ja--------
Accept-Language: en-US-------
Accept-Language: en--------
```

# Credits

 - [AcceptLanguage.php](https://github.com/BaguettePHP/http-accept-language)
   **USAMI Kenta** (MIT License)

